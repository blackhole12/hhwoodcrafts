+++
title = "Magnifying Glasses"
image = "img/magnifying-glasses/02_15130671786_o.jpg"
+++

This is not your grandfather’s magnifying glass. Function meets beauty and design with our individually turned magnifiers. From reading small text to finding a tiny part, a magnifying glass needs to be powerful and available. Hanging from a beautiful gold chain, these magnifiers are a functional fashion accessory for many nurses, teachers and crafters. Easily removed from their chains, many prefer to hang them from key chains. They are available in a variety of colorful acrylics. Choose yours today.

Available for $20.