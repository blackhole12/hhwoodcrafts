+++
title = "Custom Pens"
image = "img/pens/08_15150721511_o.jpg"
+++

Our writing instruments and styluses are individually turned out of wood, acrylic, and shed antler. No two pens will ever be exactly the same. The mechanisms of all of our pens have been chosen with style and functionality in mind. Choose a pen that reflects your style and remember that pens make a wonderful commemorative gift. Whether you are remembering the graduate, rewarding a valued employee or commemorating a special occasion, we can help you find the pen to meet your needs. Can’t decide? Why not consider a *Pen/Stylus* combination? These instruments have been extremely popular and work very well with smart phones and tablets. Corporate logos, initials and names may be engraved on our pens at your request for an additional engraving fee.

Available for order starting at **$35**.